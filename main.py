import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
# from scipy.spatial.distance import squareform, pdist
from sklearn.metrics import pairwise_distances
from sklearn.manifold import TSNE, MDS
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.cluster import KMeans, DBSCAN
from sklearn import metrics
from joblib import Memory
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import json
import pickle


cachedir = "./cachedir"
mem = Memory(cachedir)


def load_full_data():
    df = pd.read_csv("data/answer_material.csv", low_memory=False)
    return df

def load_user_data():
    df_users_filename = "data/users_full.csv"

    try:
        df_users = pd.read_csv(df_users_filename)
        print("Successfully loaded from disk.")
    
    except FileNotFoundError:
        print("Re-generating the users' time series...")
    
        usecols = (
            "eventTime", 
            "userId", 
            "textMarkerSectionId", 
            "textMarkerChapterId", 
            "userAnswerCorrect"
        )
    
        df_users = pd.read_csv("data/answer_material.csv", low_memory=False, usecols=usecols)

        # drop any rows with NaN
        df_users = df_users.dropna()

        # Turn boolean into [0,1] for easier computations later
        df_users.userAnswerCorrect = df_users.userAnswerCorrect.astype(int)

        # These are originally float, for some reason
        df_users.textMarkerSectionId = df_users.textMarkerSectionId.astype(int)
        df_users.textMarkerChapterId = df_users.textMarkerChapterId.astype(int)

        df_users.to_csv("data/users_full.csv", index=False)
    
    df_users.eventTime = pd.to_datetime(df_users.eventTime, unit="ms")
    df_users.set_index("eventTime", inplace=True)

    print("----------")
    print(df_users.info())
    
    return df_users
    

def load_user_labels():
    df_labels = pd.read_csv("data/UserUniversity.csv")

    # User ID should be the index
    df_labels.set_index("ID", inplace=True)

    # Not all users have universities
    df_labels.UniversityID.fillna(0, inplace=True)
    df_labels.UniversityID = df_labels.UniversityID.astype(int)

    df_labels.UniversityID.value_counts().sort_index()
    
    return df_labels


def load_book_structure():
    df_book_str = pd.read_csv("data/BookStructure.csv")
    df_book_str.ChapterID = df_book_str.ChapterID.str.replace(',', '').astype(int)
    return df_book_str
    
def preprocess(X, metric="cosine"):
    X_tf = TfidfTransformer().fit_transform(X)
    X_tf = TruncatedSVD(n_components=50).fit_transform(X_tf)

    # D = squareform(pdist(df_usr_chp, metric="cosine"))
    D = pairwise_distances(X_tf, metric=metric, n_jobs=-1)
    
    return D

preprocess = mem.cache(preprocess)

def project(D, perplexity=50):
    return TSNE(metric="precomputed", perplexity=50).fit_transform(D)

project = mem.cache(project)


def vis_pyplot(df_proj):
    plt.figure(figsize=(8,8))
    for label, points in df_proj.groupby("label"):
    #     alpha = 0.5 if label == 0.0 else 1.0
        alpha = 1.0
        plt.scatter(points["x"], points["y"], label=label, alpha=alpha)
    plt.legend()
    plt.show()


def vis_dash(df_proj, clusters, X, df_book_str):
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
    
    core_labels = clusters.labels_[clusters.labels_ > -1]
    # core_labels = df_proj["label"]
    cores = df_proj[clusters.labels_ > -1]
    
    noise = df_proj[clusters.labels_ == -1]

    # Create a trace
    trace1 = go.Scatter(
        x = cores["x"],
        y = cores["y"],
        customdata = cores.index,
        mode = 'markers',
        marker=dict(
                color = core_labels, #set color equal to a variable
                colorscale = 'Jet',
                # showscale=True
            )
    )
    
    trace2 = go.Scatter(
        x = noise["x"],
        y = noise["y"],
        customdata = noise.index,
        mode = 'markers',
        marker=dict(
                color = 'gray', #set color equal to a variable
                opacity = 0.5
            )
    )

    data1 = [trace1, trace2]
    # data = [trace1]
    
    layout = go.Layout(
        showlegend=False,
        yaxis=dict(
            showline=False,
            showgrid=False,
            zeroline=False,
            showticklabels=False
        ),
        xaxis=dict(
            showline=False,
            showgrid=False,
            zeroline=False,
            showticklabels=False
        ))
    
    app.layout = html.Div([
        html.Div(
            [
                dcc.Graph(
                    figure=go.Figure(data=data1, layout=layout),
                    style={'height': 800, 'width': 800},
                    id='my-graph')
            ],
            style={'display': 'inline-block'}
        ),
        html.Div(
            style={'display': 'inline-block'},
            id='output_div'
        ),        
    ])
    
    @app.callback(
        Output('output_div', 'children'),
        [Input('my-graph', 'selectedData')])
    def display_selected_data(selectedData):
        # print(json.dumps(selectedData, indent=2))
        sel_index = [x["customdata"] for x in selectedData["points"]]
        # sel_labels = df_proj.loc[sel_index, "label"]
        sel_rows = X.loc[sel_index]
        sel_means = sel_rows.mean(axis="rows").sort_values()[-10:]
        means = X.loc[:, sel_means.index].mean(axis="rows")
        # print(sel_means)
        # index = [f"_{x}" for x in sel_means.index]
        index = df_book_str.loc[sel_means.index, "ChapterName"]

        # children = []
#         for i in range(4, -1, -1):
#             data = [go.Bar(
#                 x=["selected", "global"],
#                 y=[sel_means.iloc[i], means.iloc[i]]
#             )]
#             layout = go.Layout(
#                 title=index.iloc[i],
#             )
#             children.append(dcc.Graph(
#                 figure=go.Figure(data=data, layout=layout),
#                 style={'width': 300, 'height': 150, 'margin': 0}
#             ))           

        data = [            
            go.Bar(
                x=sel_means.values,
                y=index,
                orientation = 'h',
                name="Selected",
                # mode="markers"
            ),
            go.Bar(
                x=means.values,
                y=index,
                orientation = 'h',
                name="Global"
            ),
            go.Bar(
                x=np.zeros(10),
                y=index,
                orientation = 'h',
                showlegend=False                
            )
        ]
        
        annotations = []
        for i in range(10):
            annotations.append({
                "x": 0,
                "y": index.iloc[i],
                "yshift": 20,
                "xref": "x",
                "yref": "y",
                "text": index.iloc[i],
                "xanchor": "left",
                "showarrow": False
            })
        
        layout = go.Layout(
            annotations=annotations,
            yaxis={
                "showticklabels": False
            }
        )

        children = [
            dcc.Graph(
                figure=go.Figure(data=data, layout=layout),
                style={'height': 800, 'width': 500},
                id='my-bars')
        ]
        
        return children
        
    app.run_server(debug=True)
    

if __name__ == "__main__":
    try:
        df_users = load_user_data()
        df_labels = load_user_labels()
        df_book_str = load_book_structure()
        # load_full_data()
        # exit()
    except FileNotFoundError:
        print("The input files are not in place. Contact <rafael.martins@lnu.se>.")
        exit()
    
    users_chapters = df_users.groupby(["userId", "textMarkerChapterId"])
    df_usr_chp = users_chapters.size().unstack(fill_value=0.0)
    
    N = None
    if N is not None:
        X = df_usr_chp.sample(N, random_state=12345)
    else:
        X = df_usr_chp

    suffix = str(N) if N is not None else "full"
    
    try:
        with open(f"D_{suffix}.pickle", "rb") as f:
            D = pickle.load(f)
        print(f"Successfully loaded D_{suffix} from disk.")
    except FileNotFoundError:
        print(f"D_{suffix} not found; re-generating.")
        X_tf = TfidfTransformer().fit_transform(X)
        X_tf = TruncatedSVD(n_components=50).fit_transform(X_tf)
        # D = squareform(pdist(df_usr_chp, metric="cosine"))
        D = pairwise_distances(X_tf, metric="cosine", n_jobs=-1)
        with open(f"D_{suffix}.pickle", "wb") as f:
            pickle.dump(D, f)

    try:
        with open(f"tsne_{suffix}.pickle", "rb") as f:
            proj = pickle.load(f)
        print(f"Successfully loaded tsne_{suffix} from disk.")
    except FileNotFoundError:
        print(f"tsne_{suffix} not found; re-generating.")
        #proj = TSNE(metric="precomputed", perplexity=50, verbose=999).fit_transform(D)
        proj = TSNE(metric="precomputed", perplexity=50).fit_transform(D)
        with open(f"tsne_{suffix}.pickle", "wb") as f:
            pickle.dump(proj, f)
    
    # kmeans = KMeans(n_clusters=10, random_state=12345).fit(proj)
    clusters = DBSCAN(eps=3, min_samples=10, n_jobs=-1).fit(proj)
    labels = clusters.labels_
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    print(labels)
    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    print("Silhouette Coefficient:", 
          metrics.silhouette_samples(proj, labels))
    
    # Format the results as a DataFrame, for ease-of-use below
    df_proj = pd.DataFrame(proj, index=X.index, columns=["x", "y"])
    df_proj["label"] = df_labels

    # vis_pyplot(df_proj)
    vis_dash(df_proj, clusters, X, df_book_str)
    